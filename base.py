# coding: utf-8
from collections import deque
import json
from posixpath import join as urljoin
import requests

from constants import BASE_URL, API_URL, MAX_CHUNK_SIZE


class ZCreator:
    def __init__(self, token):
        self.token = token
        self.scope = 'creatorapi'

    def do_request(self, url, params=None, method=None):
        p = {'authtoken': self.token, 'scope': self.scope}
        if params is not None:
            p.update(params)
        if method is None or method == 'GET':
            response = requests.get(url, params=p)
        elif method == 'POST':
            response = requests.post(url, params=p)
        else:
            raise ValueError(f"method must be 'GET' (default) or 'POST'")
        try:
            rv = response.json()
        except json.decoder.JSONDecodeError as e:
            e.args += (f"Invalid JSON content: {response.content}",)
            e.args += (f"URL: {url}, params: {p}",)
            raise
        if rv.get('errorlist'):
            errors = rv.get('errorlist').get('error')
            raise RuntimeError(
                f"Error(s) on API endpoint '{url}' with params '{params}'. "
                f"({errors})")

        return rv

    @property
    def shared_apps(self):
        url = urljoin(BASE_URL, 'sharedapps')
        response = self.do_request(url)
        applications = response.get('application_list').get('applications')
        return [a.get('application')[0]
                for a in applications if a.get('application')]

    @property
    def apps(self):
        url = urljoin(BASE_URL, 'applications')
        response = self.do_request(url)
        owner = response.get('result').get('application_owner')
        applications = response.get(
            'result').get('application_list').get('applications')
        apps = []
        for a in applications:
            app = a.get('application')[0]
            app.update({'application_owner': owner})
            apps.append(app)
        return apps


class ZCRecordReader:
    def __init__(self, app, view, chunk_size=MAX_CHUNK_SIZE, offset=0):
        self.app = app
        self.buffer = deque()
        self.chunk_size = chunk_size
        self.offset = offset
        self.service = app.service
        self.view = view

    def __iter__(self):
        return self

    def __next__(self):
        if not len(self.buffer):
            self._get_chunk()
        try:
            return self.buffer.popleft()
        except IndexError:
            raise StopIteration

    def _get_chunk(self):
        url = urljoin(BASE_URL, self.app.link_name, 'view', self.view)
        params = {
            'zc_ownername': self.app.owner,
            'raw': 'true',
            'startindex': self.offset,
            'limit': self.chunk_size,
            }
        self.offset += self.chunk_size
        response = self.service.do_request(url, params=params)
        next_chunk = response

        self.buffer.extend(next_chunk[list(next_chunk)[0]])


class ZCForm:
    def __init__(self, componentname, app, displayname=None, linkid=None):
        self.app = app
        self.name = componentname
        self.displayname = displayname
        self.linkid = linkid
        self.service = app.service
        self._fields = None

    def __str__(self):
        return self.displayname or self.name

    @property
    def fields(self):
        if not self._fields:
            url = urljoin(BASE_URL, self.app.link_name, self.name, 'fields')
            params = {'zc_ownername': self.app.owner}
            response = self.service.do_request(url, params=params)
            self._fields = \
                    response['application-name'][1]['form-name'][1]['Fields']
        return self._fields

    def delete(self, criteria):
        # NOTE: unusual url format using API_URL instead of BASE_URL
        # This particular functionality exists on two different endpoints
        # based on ownership of the application. Only the owner version is
        # currently implemented by this module
        url = urljoin(
                      API_URL,
                      self.app.owner,
                      'json',
                      self.app.link_name,
                      'form',
                      self.name,
                      'record',
                      'delete',
                      )
        params = {
                  'criteria': criteria,
                  'process_until_limit': 'true',
                  }
        response = self.service.do_request(url, params=params, method="POST")
        while response['formname'][1]['more_records']:
            response = self.service.do_request(url, params=params, method="POST")
        return response

    def submit(self, data):
        # NOTE: unusual url format using API_URL instead of BASE_URL
        url = urljoin(
                      API_URL,
                      self.app.owner,
                      'json',
                      self.app.link_name,
                      'form',
                      self.name,
                      'record',
                      'add',
                      )
        params = data
        response = self.service.do_request(url, params=params, method='POST')
        return response

    def update(self, criteria, fields):
        # NOTE: unusual url format using API_URL instead of BASE_URL
        # This particular functionality exists on two different endpoints
        # based on ownership of the application. Only the owner version is
        # currently implemented by this module
        url = urljoin(
                      API_URL,
                      self.app.owner,
                      'json',
                      self.app.link_name,
                      'form',
                      self.name,
                      'record',
                      'update',
                      )
        params = {
                  'criteria': criteria,
                  'process_until_limit': 'true',
                  }
        if fields.keys() & params.keys():
            raise ValueError(
                f"'fields' may not contain any key used by the api itself: "
                f"(ie. any of: {params.keys()})")
        params.update(fields)
        return self.service.do_request(url, params=params, method="POST")


class ZCApp:
    def __init__(self, link_name, service,
                 application_owner=None, sharedBy=None, *args, **kwargs):
        self.access = kwargs.get('access')
        self.name = kwargs.get('application_name') or link_name
        self.owner = self._get_application_owner(
            application_owner, sharedBy)
        self.created_time = kwargs.get('created_time')
        self.dateformat = kwargs.get('dateformat')
        self.link_name = link_name
        self.service = service
        self.shared_with = kwargs.get('shared_with')
        self.time_zone = kwargs.get('time_zone')

    def view_reader(self, view, *args, **kwargs):
        return ZCRecordReader(self, view, *args, **kwargs)

    @property
    def forms_and_views(self):
        url = urljoin(BASE_URL, self.link_name, 'formsandviews')
        params = {'zc_ownername': self.owner}
        response = self.service.do_request(url, params=params)
        return response.get('application-name')[1]

    def form(self, componentname, displayname=None, linkid=None):
        return ZCForm(componentname, self, displayname, linkid)

    def _get_application_owner(self, application_owner, sharedBy):
        if application_owner is None and sharedBy is None:
            raise TypeError('Need application_owner or sharedBy argument')
        if application_owner is not None and sharedBy is not None:
            raise TypeError('Cannot have both application_owner and sharedBy argument')
        return application_owner or sharedBy

