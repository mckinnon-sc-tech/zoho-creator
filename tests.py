from collections.abc import Iterable
import json
import unittest
from unittest.mock import patch, Mock, MagicMock

from base import ZCreator, ZCApp, ZCRecordReader
from constants import BASE_URL


TOKEN = 'faketoken'
SCOPE = 'creatorapi'

with open('fixtures/shared_apps.json', 'r') as f:
    shared_apps_json = json.load(f)

with open('fixtures/apps_list.txt', 'r') as f:
    apps_list = []
    for line in f:
        apps_list.append(json.loads(line))

with open('fixtures/shared_apps_list.txt', 'r') as f:
    shared_apps_list = []
    for line in f:
        shared_apps_list.append(json.loads(line))

with open('fixtures/apps.json', 'r') as f:
    apps_json = json.load(f)

with open('fixtures/delete.json', 'r') as f:
    delete_json = json.load(f)

with open('fixtures/edit_records.json', 'r') as f:
    edit_records_json = json.load(f)

with open('fixtures/error.json', 'r') as f:
    error_json = json.load(f)

with open('fixtures/form_fields.json', 'r') as f:
    form_fields_json = json.load(f)

with open('fixtures/forms_and_views.json', 'r') as f:
    forms_and_views_json = json.load(f)

with open('fixtures/forms_and_views_response.json', 'r') as f:
    forms_and_views_response = json.load(f)

with open('fixtures/form_submit.json', 'r') as f:
    form_submit_json = json.load(f)

with open('fixtures/form_fields_list.txt', 'r') as f:
    form_fields_list = []
    for line in f:
        form_fields_list.append(json.loads(line))

with open('fixtures/record.json', 'r') as f:
    record_json = json.load(f)


class ZCreatorTestCase(unittest.TestCase):
    """
    Test Zoho Creator class
    """
    def setUp(self):
        self.zc = ZCreator(token=TOKEN)

    def test_init_sets_token_on_object(self):
        self.assertEqual(TOKEN, self.zc.token)

    @patch('requests.get')
    def test_do_request_calls_requests_get_with_url(self, mock_requests_get):
        mock_requests_get.return_value = Mock(ok=True)
        mock_requests_get.return_value.json.return_value({})
        mock_requests_get.return_value.json.return_value.get.return_value = None
        url = 'http://www.example.com'
        params = {'authtoken': TOKEN, 'scope': SCOPE}
        self.zc.do_request(url)
        assert mock_requests_get.get.called_once_with(url)

    @patch('requests.get')
    def test_do_request_calls_requests_get_with_url_and_params(self, mock_requests_get):
        mock_requests_get.return_value = Mock(ok=True)
        mock_requests_get.return_value.json.return_value({})
        mock_requests_get.return_value.json.return_value.get.return_value = None
        url = 'http://www.example.com'
        params = {'authtoken': TOKEN, 'scope': SCOPE}
        self.zc.do_request(url, params=params)
        assert mock_requests_get.get.called_once_with(url, params)

    @patch('requests.post')
    def test_do_request_calls_requests_post_with_url_params_and_method(self, mock_requests_post):
        mock_requests_post.return_value = Mock(ok=True)
        mock_requests_post.return_value.json.return_value({})
        mock_requests_post.return_value.json.return_value.get.return_value = None
        url = 'http://www.example.com'
        params = {'authtoken': TOKEN, 'scope': SCOPE}
        self.zc.do_request(url, params=params, method='POST')
        assert mock_requests_post.called_once_with(url, params)

    @patch('requests.post')
    @patch('requests.get')
    def test_do_request_raises_valueerror_on_invalid_method(self, mock_requests_get,
                                                            mock_requests_post):
        url = 'http://www.example.com'
        params = {'authtoken': TOKEN, 'scope': SCOPE}
        with self.assertRaises(ValueError):
            self.zc.do_request(url, params=params, method='FAKE')
        assert not mock_requests_get.get.called
        assert not mock_requests_post.post.called

    @patch('requests.get')
    def test_do_request_raises_error_on_error_response(self, mock_requests_get):
        mock_requests_get.return_value = Mock(ok=True)
        mock_requests_get.return_value.json.return_value(error_json)
        url = 'http://www.example.com'
        params = {'authtoken': TOKEN, 'scope': SCOPE}

        with self.assertRaises(RuntimeError):
            self.zc.do_request(url, params=params)

        mock_requests_get.assert_called_with(url, params=params)


    @patch('base.ZCreator.do_request')
    def test_shared_apps_property(self, mock_do_request):
        mock_do_request.return_value = shared_apps_json

        response = self.zc.shared_apps

        self.assertListEqual(response, shared_apps_list)

        mock_do_request.assert_called_with('https://creator.zoho.com/api/json/sharedapps')

    @patch('base.ZCreator.do_request')
    def test_apps_property(self, mock_do_request):
        mock_do_request.return_value = apps_json

        response = self.zc.apps

        self.assertListEqual(response, apps_list)

        mock_do_request.assert_called_with('https://creator.zoho.com/api/json/applications')

class ZCreatorAppTestCase(unittest.TestCase):
    """
    Test Zoho Creator Application class
    """
    @patch('base.ZCreator.do_request')
    def setUp(self, mock_do_request):
        mock_do_request.return_value = shared_apps_json
        self.zc = ZCreator(token=TOKEN)
        self.zc_app = ZCApp(service=self.zc, **self.zc.shared_apps[0])

    @patch('base.ZCreator.do_request')
    def test_view_reader(self, mock_do_request):
        mock_do_request.return_value = apps_json

        reader = self.zc_app.view_reader('Home')
        # reader is lazy, make it do a call
        next(reader)

        mock_do_request.assert_called_with(
            'https://creator.zoho.com/api/json/assets-budgets/view/Home',
            params={'zc_ownername': 'zoho_blake265',
                    'raw': 'true',
                    'limit': 200,
                    'startindex': 0,
                    })

    @patch('base.ZCreator.do_request')
    def test_form(self, mock_do_request):
        mock_do_request.return_value = form_fields_json

        form = self.zc_app.form('Budget_List')
        form_fields = form.fields

        self.assertListEqual(form_fields, form_fields_list)
        mock_do_request.assert_called_with(
            'https://creator.zoho.com/api/json/assets-budgets/Budget_List/fields',
            params={'zc_ownername': 'zoho_blake265'})

    @patch('base.ZCreator.do_request')
    def test_form_submit(self, mock_do_request):
        mock_do_request.return_value = form_submit_json

        form = self.zc_app.form('Expenses')
        response = form.submit({'PO_Ref': 'another test'})

        mock_do_request.assert_called_with(
            'https://creator.zoho.com/api/zoho_blake265/json/assets-budgets/form/Expenses/record/add',
            params={'PO_Ref': 'another test'},
            method='POST',
            )
        self.assertDictEqual(form_submit_json, response)

    @patch('base.ZCreator.do_request')
    def test_delete_records(self, mock_do_request):
        mock_do_request.return_value = delete_json

        form = self.zc_app.form('Expenses')
        response = form.delete(
                               criteria='PO_Ref == "please delete me test"',
                               )
        mock_do_request.assert_called_with(
            'https://creator.zoho.com/api/zoho_blake265/json/assets-budgets/form/Expenses/record/delete',
            params={
                    'criteria': 'PO_Ref == "please delete me test"',
                    'process_until_limit': 'true',
                    },
            method="POST",
            )
        self.assertDictEqual(delete_json, response)

    @patch('base.ZCreator.do_request')
    def test_update_records(self, mock_do_request):
        mock_do_request.return_value = edit_records_json

        form = self.zc_app.form('Expenses')
        response = form.update(
                               criteria='PO_Ref == "another test"',
                               fields={'Description': 'test description'},
                               )

        mock_do_request.assert_called_with(
            'https://creator.zoho.com/api/zoho_blake265/json/assets-budgets/form/Expenses/record/update',
            params={
                    'criteria': 'PO_Ref == "another test"',
                    'Description': 'test description',
                    'process_until_limit': 'true',
                    },
            method="POST",
            )
        self.assertDictEqual(edit_records_json, response)

    @patch('base.ZCreator.do_request')
    def test_forms_and_views(self, mock_do_request):
        mock_do_request.return_value = forms_and_views_json

        forms_and_views = self.zc_app.forms_and_views

        self.assertDictEqual(forms_and_views, forms_and_views_response)

        mock_do_request.assert_called_with(
            'https://creator.zoho.com/api/json/assets-budgets/formsandviews',
            params={'zc_ownername': 'zoho_blake265'})


class ZCreatorRecordReaderTestCase(unittest.TestCase):
    """
    Test Zoho Creator RecordReader class
    """
    @patch('base.ZCreator.do_request')
    def setUp(self, mock_do_request):
        mock_do_request.return_value = shared_apps_json

        self.zc = ZCreator(token=TOKEN)
        self.zc_app = ZCApp(service=self.zc, **self.zc.shared_apps[0])
        mock_do_request.return_value = record_json
        self.zc_rec_read = ZCRecordReader(self.zc_app, 'Budget')

    def test_reader_is_iterable(self):
        self.assertIsInstance(self.zc_rec_read, Iterable)
