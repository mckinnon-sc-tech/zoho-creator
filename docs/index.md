# Python interface for the Zoho Creator REST API

This project is a lightweight wrapper for the [Zoho Creator REST API](https://www.zoho.com/creator/help/api/rest-api/understand-rest-api.html). Created as a python interface allowing us to create simple scripts to import and export data in/out of our Zoho Creator app. 

With few excpetions, this module doesn't (currently) do anything too clever with Zoho Creator's REST API responses, usually just handing them back to you as a python construct (eg. a dict or a list), or raising the appropriate error. For details about the structures you can expect back, see the [Zoho REST API documentation](https://www.zoho.com/creator/help/api/rest-api/understand-rest-api.html).

## Getting started

### Install

Use pip to install directly from the git repo:

	pip install git+git://gitlab.com/mckinnon-sc-tech/zoho-creator.git

See the source on our [gitlab.com](https://gitlab.com/mckinnon-sc-tech/zoho-creator) page.


### Generate a token

Generate a token from the [Zoho Creator auth token generator page](https://accounts.zoho.com/apiauthtoken/create?SCOPE=ZohoCreator/creatorapi).

NOTE: This package assumes you are the _owner_ of the Zoho Creator application. It is therefore important that the owner generate the token. This may be fixed in future versions, however Zoho do not make all features available from a shared account.

### Get app URL details

Using your application, you will notice that your address bar will have a url something like: `https://app.zohocreator.com/zoho_name123/my-special-app#Form:FormName`. The url is broken down like this:

* `zoho_name123` is the **application owner name** - you will need this!
* `my-special-app` is your **app link name** - you will need this too! (not to be confused with your app name, which is the human readable version)
* `Form` specifies that we are looking at an input form (This could be `Report`, which would instead be displaying a report)
* `FormName` specifies which form (or report)

### Example Usage

``` python3
	TOKEN = 'Your Generated Token'
	
	service = ZCreator(TOKEN)
	app = ZCApp(link_name='my-special-app',  application_owner='zoho_name123', serivce=service)
	
	# json format from the REST API
	print(app.forms_and_views)

	form = app.form('FormName')

	# json format from the REST API (shows the fields with their types, requirements, etc.)
	print(form.fields)

	submission_response = form.submit(
		{'My_Field': 'please delete me', 'My_Other_Field': 'my other data'})

	# json format from the REST API (shows success or not)
	print(submission_response)

	# NOTE: You MUST use double inner quotes for the criteria
	# NOTE: Currently there is a limit of 200 records; further calls do
	# nothing but waste your daily API hit limit!
	update_response = form.update(
		criteria='My_Field == "please delete me"',
		fields={'My_Other_Field': 'Hi there!'},
		)

	# NOTE: You MUST use double inner quotes for the criteria
	deletion_response = form.delete(criteria='My_Field == "please delete me"')

	# json format from the REST API (shows success or not)
	print(deletion_response)

	# view reader is lazy, calling the API only if needed
	for record in app.view_reader('ViewName'):
		print(record)
	
```

